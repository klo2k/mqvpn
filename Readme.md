# Connect to data-centre VPN

# Setup
1. Install OpenConnect, `gp-saml-gui` (needed for MFA):
    ```
    apt install -y openconnect
    pip3 install https://github.com/dlenski/gp-saml-gui/archive/master.zip
    ```

2. Connect with the script - log in when prompted
    ```
    ./mqvpn start
    ```

3. Stop VPN
    ```
    ./mqvpn stop
    ```
